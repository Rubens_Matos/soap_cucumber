Given("I have a wsdl location") do
    @wsdl_location = $api['url']
end
  
Given("I have created a Savon client") do
    @client = Savon::Client.new(wsdl: @wsdl_location)
end
  
Then("I should see the operations available") do
    puts @client.operations
end